Rails.application.routes.draw do
  # devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  # root to: "posts#index"
  # resources :posts
  # resources :comments, only: [:new, :create]

  # resources :comments, only: [:destroy, :edit, :update]
  # get "forbidden", to: 'posts#forbidden'
  # resources :likes, only: [:create, :destroy]


  namespace :api, defaults: { format: :json } do
    namespace :v1 do

      resources :posts, only: [:index, :show, :create, :destroy, :update]
      resources :items, only: [:index, :show, :create, :destroy, :update]
      resources :comments, only: [:create, :destroy, :show, :index]
      resources :likes, only: [:create, :destroy, :index, :show]
      resources :carts, only: [:create, :destroy, :show, :update]
      resources :cart_items, only: [:create, :destroy, :update]

      get 'carts/:id/submit', to: 'carts#submit'
      get 'carts/:id/info', to: 'carts#info'

      get 'category/:id/:info', to: 'categories#info', constraints: lambda { |req| %w[tax_fee min_legal_age discount code].include? req.params[:info]}
    end
  end

  devise_for :users,
             controllers: {
               sessions: 'users/sessions',
               registrations: 'users/registrations'
             }
  get '/member-data', to: 'members#show'

end
