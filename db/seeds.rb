# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
require 'faker'
CartItem.destroy_all
Comment.destroy_all
Post.destroy_all
Item.destroy_all
Like.destroy_all
User.destroy_all
Cart.destroy_all


# Dir[File.join(Rails.root, 'db', 'seeds', '*.rb')].sort.each do |seed|
#   load seed
# end




Dir[File.join(Rails.root, 'db', 'seeds', '*.rb')].sort.each do |file|
  puts "Processing #{file.split('/').last}"
  load file
end







# user = User.create(email: "test@email.com", password: "666666", username: "testusername")

# post = Post.new( title: "test post title", content: "test content title", user: user )
# if post.save
#   puts "post saved"
# else
#   puts "post not saved"
# end

# post_like = Like.new( user: user, likeable_type: "Post", likeable_id: "#{post.id}")

# if post_like.save
#   puts "post like saved"
# else
#   puts "post like not saved"
# end


# comment = Comment.new( content: "post comment content", commentable_type: "Post", commentable_id: "#{post.id}", user_id: "#{user.id}")
# if comment.save
#   puts "post comment saved"
# else
#   puts "post comment not saved"
# end

# comment_like = Like.new( user: user, likeable_type: "Comment", likeable_id: "#{comment.id}" )

# if comment_like.save
#   puts "comment like saved"
# else
#   puts "comment like not saved"
# end

# item = Item.new( type: "Beer", title: "budlight", description: "Redneck Beer", user: user )
# if item.save
#   puts "item saved"
# else
#   puts "item not saved"
# end

# item_like = Like.new( user: user, likeable_type: "Item", likeable_id: "#{item.id}" )

# if item_like.save
#   puts "item like saved"
# else
#   puts "item like not saved"
# end

# comment = Comment.new( content: "item comment content", commentable_type: "Item", commentable_id: "#{item.id}", user_id: "#{user.id}")
# if comment.save
#   puts "item comment saved"
# else
#   puts "item comment not saved"
# end



# # User.create(email: "test@email.com", password: "666666", username: "testusername")
# # post = Post.new(title: "test post title", content: "test content",  )





# # 4.times do
# #   user = User.new(email: Faker::Internet.free_email, password: Devise.friendly_token, username: Faker::GreekPhilosophers.name)
# #   user.save
# #   puts "user #{user.id} created"
# #   rand(1..5).times do
# #     post = Post.new(title: Faker::GreekPhilosophers.quote, content: Faker::Lorem.paragraphs )
# #     post.user = user
# #     rand(2..6).times do
# #       comment = Comment.new(content: Faker::ChuckNorris.fact, user: user)
# #       comment.save
# #       puts "comment #{comment.id} created"
# #     end
# #     post.status = :active
# #     post.save
# #     puts "post #{post.id} created"
# #   end

# # end
