class AddIndexToItemsOnDiscountType < ActiveRecord::Migration[7.0]
  def change
    add_index :items, "(data->'discount_type')", name: 'index_items_on_discount_type'
  end
end
