class AddOriginToItem < ActiveRecord::Migration[7.0]
  def change
    add_column :items, :origin, :string
  end
end
