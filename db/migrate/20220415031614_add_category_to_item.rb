class AddCategoryToItem < ActiveRecord::Migration[7.0]
  def change
    add_reference :items, :category, foriegn_key: true, null: false
  end
end
