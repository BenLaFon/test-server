class AddIndexToItemsOnTitle < ActiveRecord::Migration[7.0]

  def change
    add_index :items, "(data->'tags')", using: :gin, name: 'index_items_on_tags'
  end

end
