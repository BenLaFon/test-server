class AddDataToItems < ActiveRecord::Migration[7.0]
  def change
    add_column :items, :data, :jsonb, null: false, default: '{}'
  end
end
