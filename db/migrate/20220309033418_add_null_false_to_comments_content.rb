class AddNullFalseToCommentsContent < ActiveRecord::Migration[7.0]
  def change
    remove_column :comments, :content
    add_column :comments, :content, :string, null: false
  end
end
