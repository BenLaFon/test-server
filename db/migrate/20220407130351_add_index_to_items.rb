class AddIndexToItems < ActiveRecord::Migration[7.0]

  def change
    add_index :items, "(data->'price')", using: :gin, name: 'index_items_on_price'
  end
end
