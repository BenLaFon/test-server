require 'faker'

Item.destroy_all

discount_type = ["percent", "amount"]
tags = ["new", "top deal", "recommended", "best seller" ]
100.times do
  item = Item.new(
    category_id: Category.last.id,
    title: Faker::Beer.name,
    description: Faker::Beer.style,
    user_id: 1,
    origin: Faker::Internet.url,
    data: {
    price: rand(10..100),
    discount_type: "percent",
    discount_amount: rand(0..9),
    tags: ["new", "top deal", "recommended", "best seller" ]
    })
    item.save
  puts "Item #{item.id} created "
end
