require "json"

Comment.destroy_all

filepath = "db/sources/004_comments.json"

raw_data = File.read(filepath)

parsed_data = JSON.parse(raw_data)

Comment.create(parsed_data)
