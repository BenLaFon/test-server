require "json"

User.destroy_all

filepath = "db/sources/001_users.json"

raw_data = File.read(filepath)

user_data = JSON.parse(raw_data)

User.create(user_data)
