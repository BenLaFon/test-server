require 'faker'
Category.destroy_all

%w[Beer Wine Cocktail].each do |cat|
  Category.create(
    name: cat,
    description: Faker::Beer.name,
    rules: {
      tax_fee: 0.08,
      min_legal_age: 21,
      discount_type: %w[percent amount].sample,
      discount_amount: rand(0...15),
      code: rand(0..9999)
    }
  )
end
