require "json"

Cart.destroy_all

filepath = "db/sources/006_carts.json"

raw_data = File.read(filepath)

parsed_data = JSON.parse(raw_data)

Cart.create(parsed_data)
