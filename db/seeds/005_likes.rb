require "json"

Like.destroy_all

filepath = "db/sources/005_likes.json"

raw_data = File.read(filepath)

parsed_data = JSON.parse(raw_data)

Like.create(parsed_data)
