require 'benchmark'
require_relative '../../config/environment'

desc 'Runs the benchmark'

task :benchmark do
  Benchmark.bmbm do |x|
    x.report("Price:")   { Item.all.where("data -> 'price' >= ?", "90") }
    x.report("Tags:") { Item.all.where("data -> 'tags' ?| ['new']") }
    x.report("Type:")  { Item.all.where("data -> 'price' >= 90") }
  end
end
