
require 'benchmark'
require_relative '../../config/environment'

desc 'Compares the indexes'

task :compare do

  ActiveRecord::Base.connection.execute("DROP INDEX IF EXISTS index_items_on_tags")
  ActiveRecord::Base.connection.execute("DROP INDEX IF EXISTS index_items_on_price")
  ActiveRecord::Base.connection.execute("DROP INDEX IF EXISTS index_items_on_discount_type")

  Benchmark.bmbm do |x|
    x.report("Tags with no index:") { Item.all.where("data -> 'tags' ?| ['new']") }
    x.report("Pric with no index:") { Item.all.where("data -> 'price' >= 90") }
    x.report("Type with no index:") { Item.all.where("data -> 'discount_type' = 'amount'") }
  end

  %w[GIN BTREE].each do |index|
    Benchmark.bmbm do |x|

      ActiveRecord::Base.connection.execute("CREATE INDEX IF NOT EXISTS index_items_on_tags ON items USING #{index} ((data->'tags')) ")
      x.report("Tags with #{index}:") { Item.all.where("data -> 'tags' ?| ['new']") }
      ActiveRecord::Base.connection.execute("DROP INDEX IF EXISTS index_items_on_tags")

      ActiveRecord::Base.connection.execute("CREATE INDEX IF NOT EXISTS index_items_on_price ON items USING #{index} ((data->'price')) ")
      x.report("Pric with #{index}:") { Item.all.where("data -> 'price' >= 90") }
      ActiveRecord::Base.connection.execute("DROP INDEX IF EXISTS index_items_on_price")

      ActiveRecord::Base.connection.execute("CREATE INDEX IF NOT EXISTS index_items_on_discount_type ON items USING #{index} ((data->'discount_type')) ")
      x.report("Type with #{index}:") { Item.all.where("data -> 'discount_type' = 'amount'") }
      ActiveRecord::Base.connection.execute("DROP INDEX IF EXISTS index_items_on_discount_type")
      puts "   "
      puts "   "

    end
  end

  ActiveRecord::Base.connection.execute("CREATE INDEX IF NOT EXISTS index_items_on_tags ON items USING GIN ((data->'tags')) ")
  ActiveRecord::Base.connection.execute("CREATE INDEX IF NOT EXISTS index_items_on_price ON items USING GIN ((data->'price')) ")
  ActiveRecord::Base.connection.execute("CREATE INDEX IF NOT EXISTS index_items_on_discount_type ON items USING BTREE ((data->'discount_type')) ")

end
