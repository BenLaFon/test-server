class Item < ApplicationRecord
  enum status: [:active, :inactive, :forbidden]
  belongs_to :user
  belongs_to :category
  has_many :comments, as: :commentable
  has_many :likes, as: :likeable
  has_many :cart_items


  def discounted_price
    if self.data.dig("discount_type") == "percent"
      price = self.data.dig("price")
      discount = (100 - self.data.dig("discount_amount"))/100.to_f
      price * discount
    elsif self.data.dig("discount_type") == "amount"
      self.data.dig("price") - self.data.dig("discount_amount")
    else
      self.data.dig("price")
    end
  end



  def subtract_quantity(amount)
    self.quantity = quantity - amount if quantity >= amount
    self.save
  end

  def extract_host
    url = self.origin
    self.origin = url.match(/^(https?:\/\/)?(www\.)?([^\/]+)/)[3]
  end

  %w[tax_fee min_legal_age discount code].each do |m|
    define_method(m) do
      self.category.rules[m]
    end
  end

  %w[tags price discount_type discount_amount]. each do |m|
    define_method(m) do
      self.data.dig(m)
    end
  end

end
