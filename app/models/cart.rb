class Cart < ApplicationRecord
  has_many :cart_items
  has_many :items, through: :cart_items
  enum status: [:unsubmitted, :submitted, :abandoned]


  def total_price
    array = []
    self.cart_items.each do |cart_item|
      price = Item.find_by(id: cart_item.item_id).data.dig("price")
      quantity = cart_item.quantity
      array.push(price * quantity)
    end
    array.sum
  end

  

end
