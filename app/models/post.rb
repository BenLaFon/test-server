class Post < ApplicationRecord
  enum status: [:active, :inactive, :forbidden]
  belongs_to :user
  has_many :comments, as: :commentable
  has_many :likes, as: :likeable


end
