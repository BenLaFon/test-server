class CartItem < ApplicationRecord
  belongs_to :item
  belongs_to :cart

  def item_code
    category_code = self.item.category.rules.dig("code")
    return "#{category_code}-#{self.item_id}"
  end

  def price
    if self.item.category.rules.dig("discount_type") && self.item.category.rules.dig("discount_amount")
      self.discounted_price
    else
      self.item.discounted_price
    end
  end

  def discounted_price

    if self.item.category.rules.dig("discount_type") == "percent"
      price = self.item.data.dig("price")
      discount = (100 - self.item.category.rules.dig("discount_amount"))/100.to_f
      price * discount
    elsif self.item.category.rules.dig("discount_type") == "amount"
      self.item.data.dig("price") - self.item.category.rules.dig("discount_amount")
    else
      self.item.data.dig("price")
    end

  end

end
