class Category < ApplicationRecord
  has_many :items


  %w[tax_fee min_legal_age discount code].each do |m|
    define_method(m) do
      self.rules.dig(m)
    end
  end


end
