class Api::V1::LikesController < Api::V1::BaseController
  before_action :set_like, only: [:destroy, :show]

  def create
    @like = Like.new(like_params)
    if @like.save
      render json: @like, status: :created
    else
      render json: @like.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @like.destroy
      render json: @like, status: :created
    else
      render json: @like.errors, status: :unprocessable_entity
    end
  end

  def show

  end

  def index
    @like = Like.all
  end


  private

  def like_params
    params.require(:like).permit(:id, :likeable_id, :likeable_type, :user_id)
  end

  def set_like
    @like = Like.find_by(id: params[:id])
  end

end
