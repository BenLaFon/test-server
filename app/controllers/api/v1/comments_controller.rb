class Api::V1::CommentsController < Api::V1::BaseController
  before_action :set_comment, only: [:destroy, :show]

  def create
    @comment = Comment.new(comment_params)
    if @comment.save
      render json: @comment, status: :created
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @comment.status = "forbidden"
    if @comment.save
      render json: @comment, status: :created
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  def show

  end

  def index
    @comments = Comment.all
  end


  private

  def comment_params
    params.require(:comment).permit(:id, :content, :commentable_id, :commentable_type, :user_id)
  end

  def set_comment
    @comment = Comment.find(params[:id])
  end

end
