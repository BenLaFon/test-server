class Api::V1::CartItemsController < Api::V1::BaseController
before_action :set_cart_item, only: [:destroy, :update]
  def create
    @cart_item = CartItem.new(cart_item_params)
    if @cart_item.save
      render json: @cart_item, status: :created
    else
      render json: @cart_item.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @cart_item = CartItem.find_by(id: params[:id])
    if @cart_item.destroy
      render json: @cart_item, status: :ok
    else
      render json: @cart_item.errors, status: :unprocessable_entity
    end
  end

  def update
    @cart_item.update(cart_item_params)

    if @cart_item.save
      render json: @cart_item, status: :ok
    else
      render json: @cart_item.errors, status: :unprocessable_entity
    end

  end



  private

  def cart_item_params
    params.require(:cart_item).permit(:quantity, :cart_id, :item_id, :id)
  end

  def set_cart_item
    @cart_item = CartItem.find_by(id: params[:id])
  end

end
