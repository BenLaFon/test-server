class Api::V1::ItemsController < Api::V1::BaseController
  before_action :set_item, only: [:show, :update, :destroy]

  def index

    @items = Item.all
    if params[:query].present?
      @items.where!("data ->>'discount_type' = ?", "#{params[:query]["discount_type"]}") if params[:query].include?("discount_type")
      @items.where!("data -> 'price' >= ?", "#{params[:query]["price_range"][0]}").where!("data -> 'price' <= ?", "#{params[:query]["price_range"][1]}") if params[:query].include?("price_range")
      @items.where!("data -> 'tags' ?| array[:tags]", tags: params[:query]["tags"]) if params[:query].include?("tags")

    end
  end

  def create
    @item = Item.new(item_params)
    @item.extract_host

    if @item.save
      render json: @item, status: :created
    else
      render json: @item.errors, status: :unprocessable_entity
    end
  end

  def update
    @item.update(item_params)
    @item.extract_host  if @item.origin
    if @item.save
      render json: @item, status: :created
    else
      render json: @item.errors, status: :unprocessable_entity
    end
  end

  def show

  end

  def destroy
    @item.status = 'forbidden'
    if @item.save
      render json: @item, status: :created
    else
      render json: @item.errors, status: :unprocessable_entity
    end
  end

  private

  def item_params
    params.require(:item).permit(:title, :origin, :description, :user_id, :category_id, :status, data: {})
  end

  def set_item
    @item = Item.find_by(id: params[:id])
  end

end
