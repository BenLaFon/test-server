class Api::V1::CartsController < Api::V1::BaseController
before_action :set_cart, only: [:destroy, :submit, :info]

  def create
    @cart = Cart.create
  end

  def destroy
    @cart.status = "abandoned"
  end

  def submit
    cart_items = @cart.cart_items
    cart_items.each do |cart_item|
      item = Item.find_by(id: cart_item["item_id"])
      item.subtract_quantity(cart_item["quantity"])
    end
    @cart.status = "submitted"
    @cart.save
    render json: @cart, status: :ok
  end

  private

  def set_cart
    @cart = Cart.find_by(id: params[:id])
  end

  def cart_params
    params.require(:cart).permit(:id)
  end



end
