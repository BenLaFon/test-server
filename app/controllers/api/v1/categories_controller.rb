class Api::V1::CategoriesController < Api::V1::BaseController
before_action :set_category

  def info
     @result = @category.send(params[:info])
  end



  private

  def set_category
    @category = Category.find_by(id: params[:id])
  end

  def category_params
    params.require(:category).permit(:id, :info)
  end

end
