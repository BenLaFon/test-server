class LikesController < ApplicationController
  before_action :like_params

  def create
    @like = Like.new(like_params)
    @like.user = current_user
    if @like.save
      render json: @like, status: :created
    else
      render json: @like.errors, status: :unprocessable_entity
    end

    authorize @like
  end

  def destroy
    @like = Like.find_by(id: params[:id])
    @like.destroy

  end


  private



  def like_params
    params.require(:like).permit(:likeable_id, :likeable_type)
  end



end
