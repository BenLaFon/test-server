class CommentsController < ApplicationController
  before_action :set_commentable, except: :destroy

  def new
    @comment = Comment.new
    authorize @post
  end

  def create
    @comment = Comment.new(comment_params)
    @comment.user = current_user

    if @comment.save
      render json: @comment, status: :created
    else
      render json: @comment.errors, status: :unprocessable_entity
    end

    authorize @comment
  end

  def destroy
    @comment = Comment.find_by(id: params[:id])
    @post = @comment.post
    @comment.destroy
    redirect_to post_path(@post)
    authorize @comment
  end

  private

  def comment_params
    params.require(:comment).permit(:content, :commentable_id, :commentable_type, :user)
  end


  def set_commentable
    commentable_id = params[:commentable_id]
    commentable_type = params[:commentable_type]
  end

end
