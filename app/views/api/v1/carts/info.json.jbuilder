json.total_price @cart.total_price
json.cart do
  json.array! @cart.items do |item|
    json.item_name item.title
    json.item_price item.price
    json.tax_fee item.tax_fee
  end
end
