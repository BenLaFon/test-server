json.comments do
  json.array! @comments do |comment|
    json.extract! comment, :id, :user, :content, :commentable_id, :commentable_type
  end
end

