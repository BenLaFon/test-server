json.items do
  json.array! @items do |post|
    json.extract! post, :title, :description, :user_id, :status, :data
  end
end
