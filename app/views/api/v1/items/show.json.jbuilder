#  json.extract! @item, :title, :description, :user_id, :type, :status, :data
#  json.extract! @discounted_price

json.item do
  json.title @item.title
  json.discounted_price @item.discounted_price
  json.description @item.description
end
