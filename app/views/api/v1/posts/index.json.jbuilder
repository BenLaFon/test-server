json.posts do
  json.array! @posts do |post|
    json.extract! post, :id, :title, :content, :user
  end
end
