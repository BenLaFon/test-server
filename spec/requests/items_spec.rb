require 'rails_helper'

RSpec.describe "Items", type: :request do
  let!(:user) { User.create( email: "test@email.com", password: "666666", username: "testusername", id: 1)}
  let!(:category) {Category.create( name: "Burr", description: Faker::Beer.name, rules: { tax_fee: 0.06, min_legal_age: 21, discount: "percent", code: rand(0..9999)} )}
  let!(:category1) {Category.create( name: "Burr2", description: Faker::Beer.name, rules: { tax_fee: 0.06, min_legal_age: 21, discount: "percent", code: rand(0..9999)} )}
  let!(:item) {Item.create(category: category, title: "BudLight", description: "cheap beer", user_id: 1, id: 30, data: { "price": 5, "discount_type": "percent", "discount_amount": 20, "tags": ["top deal"]})}
  let!(:item_2) {Item.create(category: category, title: "Cors", description: "cheaper beer", user_id: 1, id: 40, data: {  "price": 5, "discount_type": "amount", "discount_amount": 2, "tags": ["new"]})}
  let!(:item_3) {Item.create(category: category1, title: "HouseRed", description: "cheaper wine", user_id: 1, id: 50, data: { "price": 20, "discount_type": "amount", "discount_amount": 2, "tags": ["top deal", "new"]})}
  let!(:params) {{ item: { id: 90000, category_id: 5000, title: "BudLights", description: "cheap beers", origin: "www.facebook.com/stuff_to_delete", user_id: 1, data: { price: 5, discount_type: "amount", discount_amount: 2, tags: ["top deal"] }}}}

  describe "index" do
    it "lists all Items" do

      get '/api/v1/items'

      json = JSON.parse(response.body)

      expect(json["items"].size).to eq(3)
    end

    it "Sorts items by discount type (amount)" do

      get '/api/v1/items', params: { query: {"discount_type": "amount"}}

      json = JSON.parse(response.body)

      expect(JSON.parse(response.body)["items"][0]["title"]).to eq("Cors")
    end

    it "Sorts items by discount type (percent)" do

      get '/api/v1/items', params: { query: {"discount_type": "percent"}}

      json = JSON.parse(response.body)

      expect(json["items"][0]["title"]).to eq("BudLight")
    end

    it "Sorts items by tags " do

      get '/api/v1/items', params: { query: {"tags": ["new", "top deal"]}}
      json = JSON.parse(response.body)
      expect(json["items"].size).to eq(3)
    end

    it "Sorts items by price " do

      get '/api/v1/items', params: { query: {"price_range": ["6","20"]}}

      json = JSON.parse(response.body)

      expect(json["items"][0]["title"]).to eq("HouseRed")
    end

  end

  describe "Show" do
    it "shows specific item and calculates 'percentage' discount" do

      get '/api/v1/items/30'

      r = JSON.parse(response.body)
      expect(r['item']['title']).to eq("BudLight")
      expect(r['item']["discounted_price"]).to eq(4)
    end

    it "shows specific item and calculates 'amount' discount" do

      get '/api/v1/items/40'

      r = JSON.parse(response.body)
      expect(r['item']['title']).to eq("Cors")
      expect(r['item']["discounted_price"]).to eq(3)
    end

  end

  describe "Create" do
    let!(:category) {Category.create( id: 5000, name: "Burr", description: Faker::Beer.name, rules: { tax_fee: 0.06, min_legal_age: 21, discount: "percent", code: rand(0..9999)} )}

    it "creates new item" do
      post '/api/v1/items', params: params, as: :json


      expect(JSON.parse(response.body)["origin"]).to eq("facebook.com")
      expect(response).to have_http_status(:created)
    end
  end

  describe "Update" do
    it "updates item" do

      put "/api/v1/items/#{item.id}", params: { item: {category: category, title: "BudLighter", description: "cheaper beer", user_id: 1, id: 1, origin: "https://www.website.com/item/154"}}

      expect(JSON.parse(response.body)["origin"]).to eq("website.com")
      expect(response).to have_http_status(:created)
    end
  end

  describe "destroy" do
    it "sets item status to forbidden" do

      delete "/api/v1/items/#{item.id}"

      expect(response).to have_http_status(:created)
    end
  end


end
