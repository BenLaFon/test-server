require 'rails_helper'
require 'devise/jwt/test_helpers'
require 'helpers/get_auth_headers'

RSpec.describe "Sessions", type: :request do
  let!(:user) { User.create( email: "test12@email.com", password: "66666612", username: "testusername12", id: 2)}
  let!(:auth_headers) {get_auth_headers(user)}

  it "creates new user ", type: :request do
    post '/users', params: { user: {email: "thespec@email.com", password: "password", username: "rspecusername"}}

    expect(response).to have_http_status(:ok)
  end

  it " Sign user in  ", type: :request do
    post '/users/sign_in', params: { user: {email: "thespec@email.com", password: "password", username: "rspecusername"}}

    expect(response).to have_http_status(:ok)
  end

  it "signs user out", type: :request do
    get '/users/sign_out', headers: auth_headers

    expect(response).to have_http_status(:ok)
  end

end
