require 'rails_helper'

RSpec.describe "Carts", type: :request do
  let!(:user) { User.create( email: "test@email.com", password: "666666", username: "testusername", id: 1)}
  let!(:category) {Category.create( name: "Burr", description: Faker::Beer.name, rules: { tax_fee: 0.06, min_legal_age: 21, discount: "percent", code: rand(0..9999)} )}
  let!(:item) {Item.create( quantity: 50, category: category, title: "BudLight", description: "cheap beer", user_id: 1, id: 3, data: { "price": 5, "discount_type": "percent", "discount_amount": 20, "tags": ["top deal"]})}
  let!(:item2) {Item.create( quantity: 40, category: category, title: "Cors", description: "beer", user_id: 1, id: 6, data: { "price": 5, "discount_type": "percent", "discount_amount": 20, "tags": ["top deal"]})}
  let!(:cart) {Cart.create(id: 1)}
  let!(:cart_item) { CartItem.create( id: 10, quantity: 1, cart_id: 1, item_id: 3)}
  let!(:cart_item2) { CartItem.create( id: 15, quantity: 2, cart_id: 1, item_id: 6)}




  describe "submit" do
    it "submits the cart and updates Item quantity" do
      get '/api/v1/carts/1/submit'

      expect(response).to have_http_status(:ok)
    end
  end



end
