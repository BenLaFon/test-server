require 'rails_helper'

RSpec.describe "Comments Controller", type: :request do

  it "lists all comments", type: :request do
    user = User.create(email: "test@email.com", password: "666666", username: "testusername", password_confirmation: "666666", id: 1)
    post_1 = Post.create( title: "test post title", content: "test content title", user_id: 1 )
    comment = Comment.create(content: "post comment content", commentable_id: "#{post_1.id}", commentable_type: "Post", user_id: 1, id: 1)

    get '/api/v1/comments'
    json = JSON.parse(response.body)


    expect(JSON.parse(response.body).size).to eq(1)
  end

  it "gets one specific comment", type: :request do
    user = User.create(email: "test@email.com", password: "666666", username: "testusername", password_confirmation: "666666", id: 1)
    post_1 = Post.create( title: "test post title", content: "test content title", user_id: 1 )
    comment = Comment.create(content: "post comment content", commentable_id: "#{post_1.id}", commentable_type: "Post", user_id: 1, id: 1)

    get '/api/v1/comments/1'

    json = JSON.parse(response.body)
    expect(json["id"]).to eq(1)
  end

  it "changes a comment status to forbidden", type: :request do
    user = User.create(email: "test@email.com", password: "666666", username: "testusername", password_confirmation: "666666", id: 1)
    post_1 = Post.create( title: "test post title", content: "test content title", user_id: 1 )
    comment = Comment.create(content: "post comment content", commentable_id: "#{post_1.id}", commentable_type: "Post", user_id: 1, id: 1)

    delete "/api/v1/comments/#{comment.id}"
    json = JSON.parse(response.body)
    expect(response).to have_http_status(:created)
  end

  it "creates a new comment", type: :request do
    user = User.create(email: "test@email.com", password: "666666", username: "testusername", password_confirmation: "666666", id: 1)
    post_1 = Post.create( title: "test post title", content: "test content title", user_id: 1, id: 1 )

    post '/api/v1/comments', params: { comment: {content: "testing 123", user_id: 1, id: 1, commentable_id: 1, commentable_type: "Post"}}

    expect(response).to have_http_status(:created)
  end



end
