require 'rails_helper'

RSpec.describe "Likes", type: :request do

  it "lists all likes", type: :request do
    user = User.create(email: "test@email.com", password: "666666", username: "testusername", password_confirmation: "666666", id: 1)
    post_1 = Post.create( title: "test post title", content: "test content title", user_id: 1 )
    like = Like.create(likeable_id: "#{post_1.id}", likeable_type: "Post", user_id: 1, id: 1)

    get '/api/v1/likes'
    json = response.body



    expect(JSON.parse(response.body).size).to eq(1)
  end

  it "gets one specific like", type: :request do
    user = User.create(email: "test@email.com", password: "666666", username: "testusername", password_confirmation: "666666", id: 1)
    post_1 = Post.new( title: "test post title", content: "test content title", user_id: 1, id: 1 )
    post_1.save

    like = Like.new(likeable_id: 1, likeable_type: "Post", user_id: 1, id: 1)
    like.save

    get '/api/v1/likes/1'

    json = JSON.parse(response.body)
    expect(json["id"]).to eq(1)
  end

  it "creates a new like", type: :request do
    user = User.create(email: "test@email.com", password: "666666", username: "testusername", password_confirmation: "666666", id: 1)
    post_1 = Post.create( title: "test post title", content: "test content title", user_id: 1, id: 1 )

    post '/api/v1/likes', params: { like: { user_id: 1, id: 1, likeable_id: 1, likeable_type: "Post"}}

    expect(response).to have_http_status(:created)
  end

    it "deletes like", type: :request do
    user = User.create(email: "test@email.com", password: "666666", username: "testusername", password_confirmation: "666666", id: 1)
    post_1 = Post.create( title: "test post title", content: "test content title", user_id: 1 )
    like = Like.create(likeable_id: "#{post_1.id}", likeable_type: "Post", user_id: 1, id: 1)

    delete "/api/v1/likes/#{like.id}"
    json = JSON.parse(response.body)
    expect(response).to have_http_status(:created)
  end


end
