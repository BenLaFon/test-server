require 'rails_helper'

RSpec.describe "CartItems", type: :request do
  let!(:category) {Category.create( name: "Burr", description: Faker::Beer.name, rules: { tax_fee: 0.06, min_legal_age: 21, discount: "percent", code: rand(0..9999)} )}
  let!(:user) { User.create( email: "test@email.com", password: "666666", username: "testusername", id: 1)}
  let!(:item) {Item.create(category_id: category.id, title: "BudLight", description: "cheap beer", user_id: 1, id: 3, data: { "price": 5, "discount_type": "percent", "discount_amount": 20, "tags": ["top deal"]})}
  let!(:cart) {Cart.create(id: 2)}
  let!(:cart_item) { CartItem.create( id: 10, quantity: 1, cart_id: 2, item_id: 3)}
  let!(:params) {{ cart_item: { quantity: 1, cart_id: 2, item_id: 3 }}}

  describe "create" do
    it "creates a new cart item" do
      post '/api/v1/cart_items', params: params, as: :json

      expect(response).to have_http_status(:created)
    end
  end

  describe 'update' do
    it 'updates a cart_item' do
      patch '/api/v1/cart_items/10', params: { cart_item: { quantity: 15}}
      r = JSON.parse(response.body)

      expect(r["quantity"]).to eq(15)
    end
  end

  describe 'delete' do
    it 'delets a cart_item' do
      delete '/api/v1/cart_items/10'

      expect(response).to have_http_status(:ok)
    end
  end

end
