require 'rails_helper'


RSpec.describe "Posts Index", type: :request do


  it "lists all posts", type: :request do
    user = User.create({ "email": "test@email.com", "password": "666666", "username": "testusername", "id": 1})
    post = Post.create(title: "test post title", content: "test content", user: user, id: 1)
    get '/api/v1/posts'

    expect(JSON.parse(response.body).size).to eq(1)
  end

end

RSpec.describe "Posts Show", type: :request do

  it "gets info on specific post ", type: :request do

    user = User.create({ "email": "test@email.com", "password": "666666", "username": "testusername", "id": 1})
    post = Post.create(title: "test post title", content: "test content", user: user, id: 1)


    get '/api/v1/posts/1'

    json = JSON.parse(response.body)
    expect(json["id"]).to eq(1)
  end
end

RSpec.describe "Posts Create", type: :request do
  it "Creates a new post ", type: :request do
    user = User.create({ "email": "test@email.com", "password": "666666", "username": "testusername", "id": 1})
    post '/api/v1/posts', params: { post: {content: "testing 123", title: "the title", user_id: 1}}

    expect(response).to have_http_status(:created)
  end
end

RSpec.describe "Posts Update", type: :request do
  it "Updates a post ", type: :request do
    user = User.create({ "email": "test@email.com", "password": "666666", "username": "testusername", "id": 1})
    new_post = Post.create(title: "original title", content: "original test content", user: user, id: 2)
    put "/api/v1/posts/#{new_post.id}", params: { post: {content: "much better test content", title: "the much better title", user_id: 1, id: 2}}

    expect(response).to have_http_status(:created)
  end
end

RSpec.describe "Posts Destroy", type: :request do

  it "Sets post status to forbidden ", type: :request do

    user = User.create({ "email": "test@email.com", "password": "666666", "username": "testusername", "id": 1})
    post = Post.new(title: "test post title", content: "test content", user: user, id: 1)
    post.save

    delete '/api/v1/posts/1'

    json = JSON.parse(response.body)
    expect(response).to have_http_status(:created)
  end
end


