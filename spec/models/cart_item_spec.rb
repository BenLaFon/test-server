require 'rails_helper'

RSpec.describe CartItem, type: :model do
  let!(:category) {Category.create( name: "Burr", description: Faker::Beer.name, rules: { tax_fee: 0.06, min_legal_age: 21, discount: "percent", code: rand(0..9999)} )}
  let!(:cart) {Cart.create(id: 1)}
  let!(:item) {Item.create(category: category, quantity: 50, title: "BudLight", description: "cheap beer", user_id: 1, id: 30, data: { "price": 5, "discount_type": "percent", "discount_amount": 20, "tags": ["top deal"]})}
  let!(:cart_item) {CartItem.create(quantity: 2, item_id: 30, cart_id: 1)}


  it{ is_expected.to belong_to(:item) }
  it{ is_expected.to belong_to(:cart) }



end
