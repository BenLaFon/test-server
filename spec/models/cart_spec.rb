require 'rails_helper'

RSpec.describe Cart, type: :model do
  let!(:category) {Category.new( id: 1, name: "Burr", description: "BEEEEEEEEER", rules: { tax_fee: 0.06, min_legal_age: 21, discount: "percent", code: rand(0..9999)} )}
  let!(:cart) {Cart.new(id: 1)}
  let!(:item) {Item.new(category_id: 1, quantity: 50, title: "BudLight", description: "cheap beer", user_id: 1, id: 30, data: { "price": 5, "discount_type": "percent", "discount_amount": 20, "tags": ["top deal"]})}
  let!(:cart_item) {CartItem.new(quantity: 2, item_id: 30, cart_id: 1)}

  it{ is_expected.to have_many(:cart_items) }
  it{ is_expected.to have_many(:items).through(:cart_items) }

  it 'total price' do
    byebug
    p cart.total_price
  end

end
