require 'rails_helper'
require 'faker'

RSpec.describe Item, type: :model do
  let!(:category) {Category.create( name: "Burr", description: Faker::Beer.name, rules: { tax_fee: 0.06, min_legal_age: 21, discount: "percent", code: rand(0..9999)} )}
  let!(:item) {Item.create(category: category, quantity: 50, title: "BudLight", description: "cheap beer", user_id: 1, id: 30, data: { "price": 5, "discount_type": "percent", "discount_amount": 20, "tags": ["top deal"]})}

  it{ is_expected.to have_many(:comments) }
  it{ is_expected.to have_many(:likes) }
  it{ is_expected.to belong_to(:user) }

  it 'Subtracts quantity method' do
    item.subtract_quantity(10)

    expect(item.quantity).to eq(40)
  end

  it 'Protects Item quantity from going negative' do
    item.subtract_quantity(60)

    expect(item.quantity).to eq(50)
  end

  it 'Allows Item quantity to go to zero' do
    item.subtract_quantity(50)

    expect(item.quantity).to eq(0)
  end

end

